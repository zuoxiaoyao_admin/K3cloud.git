<?php

/*
 * This file is part of the opsoft/k3cloud.
 *
 * (c) 左逍遥  zuoq@opsoft.com.cn
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace K3cloud\Calendar;

use K3cloud\Kernel\BaseClient;

class Client extends BaseClient
{
    /**
     * 创建日程
     *
     * @param array $params
     *
     * @return mixed
     */
    public function create($params)
    {
        return $this->client->postJson('topapi/calendar/create', $params);
    }
}
