<?php

/*
 * This file is part of the opsoft/k3cloud.
 *
 * (c) 左逍遥  zuoq@opsoft.com.cn
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace K3cloud\Kernel;

class BaseClient
{
    /**
     * @var \K3cloud\Application
     */
    protected $app;

    /**
     * @var \K3cloud\Kernel\Http\Client
     */
    protected $client;

    /**
     * Client constructor.
     *
     * @param \K3cloud\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->client = $this->app['client']->withAccessTokenMiddleware()->withRetryMiddleware();
    }
}
