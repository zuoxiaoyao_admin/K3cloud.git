<?php

/*
 * This file is part of the opsoft/k3cloud.
 *
 * (c) 左逍遥  zuoq@opsoft.com.cn
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace K3cloud\Blackboard;

use K3cloud\Kernel\BaseClient;

class Client extends BaseClient
{
    /**
     * 获取用户公告数据
     *
     * @param string $userid
     *
     * @return mixed
     */
    public function list($userid)
    {
        return $this->client->postJson('topapi/blackboard/listtopten', compact('userid'));
    }
}
