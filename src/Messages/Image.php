<?php

/*
 * This file is part of the opsoft/k3cloud.
 *
 * (c) 左逍遥  zuoq@opsoft.com.cn
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace K3cloud\Messages;

class Image extends Message
{
    protected $type = 'image';

    protected function transform($value)
    {
        list($mediaId) = $value;

        return ['media_id' => $mediaId];
    }
}
